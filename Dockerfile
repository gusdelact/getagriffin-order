FROM openjdk:14
COPY ./order-main-1.0.jar /
CMD ["java", "-cp", "order-main-1.0.jar", "order.main.Order"]
