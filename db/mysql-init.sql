use Orders;

DROP TABLE IF EXISTS `Orders`;
CREATE TABLE `Orders` (
  `OrderId`		int NOT NULL AUTO_INCREMENT,
  `DeptNo`		int NOT NULL,
  `GriffinId`	varchar(255) NOT NULL,
  `SaddleId`	varchar(255) NOT NULL,
  PRIMARY KEY (`OrderId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
