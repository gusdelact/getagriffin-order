package order.storytests;

import order.stub.*;
import fulfillgriffin.stub.*;
import fulfillsaddle.stub.*;

import cucumber.api.java.Before;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import static org.junit.Assert.*;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.net.URL;

public class FailureTestsIT {

	private int instance_noOfGriffinsAvail = 0;

	private String orderFactoryClassName = System.getenv("ORDER_FACTORY_CLASS_NAME");
	private OrderFactory oFac;
	private OrderStub order;

	private String fulfillGriffinFactoryClassName = System.getenv("FULFILL_GRIFFIN_FACTORY_CLASS_NAME");
	private FulfillGriffinFactory fgFac;
	private FulfillGriffinStub fg;

	private String fulfillSaddleFactoryClassName = System.getenv("FULFILL_SADDLE_FACTORY_CLASS_NAME");
	private FulfillSaddleFactory fsFac;
	private FulfillSaddleStub fs;

	private int instance_orderId = 0;

	public FailureTestsIT() {
		if (orderFactoryClassName == null) throw new Error(
			"Env variable ORDER_FACTORY_CLASS_NAME not set");
		try {
			oFac = (OrderFactory)(Class.forName(orderFactoryClassName).getDeclaredConstructor().newInstance());
			order = oFac.createOrder();
		} catch (Exception ex) {
			throw new RuntimeException("During startup", ex);
		}
		System.err.println("Constructed Order stub");

		if (fulfillGriffinFactoryClassName == null) throw new Error(
			"Env variable FULFILL_GRIFFIN_FACTORY_CLASS_NAME not set");
		try {
			fgFac = (FulfillGriffinFactory)(Class.forName(fulfillGriffinFactoryClassName).getDeclaredConstructor().newInstance());
			fg = fgFac.createFulfillGriffin();
		} catch (Exception ex) {
			throw new RuntimeException("During startup", ex);
		}
		System.err.println("Constructed FulfillGriffin stub");

		if (fulfillSaddleFactoryClassName == null) throw new Error(
			"Env variable FULFILL_SADDLE_FACTORY_CLASS_NAME not set");
		try {
			fsFac = (FulfillSaddleFactory)(Class.forName(fulfillSaddleFactoryClassName).getDeclaredConstructor().newInstance());
			fs = fsFac.createFulfillSaddle();
		} catch (Exception ex) {
			throw new RuntimeException("During startup", ex);
		}
		System.err.println("Constructed FulfillSaddle stub");
	}


	/* Scenario Add To Inventory */

	@Given("there are no more saddles")
	public void no_more_saddles() throws Exception {
		fs.haltRequisitions();
		this.instance_noOfGriffinsAvail = fg.getNoOfGriffinsAvailable();
	}

	@When("I call Provision")
	public void call_provision() throws Exception {
		try {
			this.instance_orderId = order.provision(123);
		} catch (Exception ex) { // we expect an error to occur
			System.err.println("Exception returned as expected");
		} finally {
			fs.enableRequisitions();
		}
	}

	@Then("the provision automatically cancels the griffin requisition")
	public void cancels_griffin_requisition() throws Exception {
		int noOfGsAv = fg.getNoOfGriffinsAvailable();
		if (this.instance_noOfGriffinsAvail != noOfGsAv) throw new Exception(
			"Mismatch - no of griffins available to be " + this.instance_noOfGriffinsAvail +
			" but it is " + noOfGsAv);
	}
}
