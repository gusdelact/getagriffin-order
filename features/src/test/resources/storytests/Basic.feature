@Happy
Feature: Basic

	@JustOne
	Scenario: Provision

		When I call Provision with department number 123
		Then it returns an order ID

	Scenario: Cancel

		Given I called Provision with department number 123
		When I call CancelOrder on the order
		Then it does not return an error
