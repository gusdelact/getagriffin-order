package order.stub;

/* */
public interface OrderStub {
	int provision(int deptNo) throws Exception;
	void cancelOrder(int orderId) throws Exception;
	String ping() throws Exception;
}
